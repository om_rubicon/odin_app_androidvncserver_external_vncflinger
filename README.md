# How to build the vncflinger binary

## Sources and build

First, [use repo](https://source.android.com/setup/build/downloading) to download AOSP.

I used this branch for Android 9: `android-security-9.0.0_r76`

```
source build/envsetup.sh
lunch aosp_arm64
```

Clone tigervnc and vncflinger (this repository) into `external`:

```
cd external
git clone -b orderman git@bitbucket.org:om_rubicon/odin_app_androidvncserver_external_vncflinger.git vncflinger
git clone -b orderman git clone git@bitbucket.org:om_rubicon/odin_external_tigervnc.git tigervnc
```

Go into `external/tigervnc` and run `mm`. Then go into `external/vncflinger` and also run `mm`.

## Troubleshooting

### javac

If you get `JAVAC does not match between Make and Soong`, run:
```
export JAVAC=prebuilts/jdk/jdk9/linux-x86/bin/javac
```

### flex

If you get this:

```
prebuilts/misc/linux-x86/flex/flex-2.5.39 -oout/soong/.intermediates/system/tools/aidl/libaidl-common/linux_glibc_x86_64_static/gen/lex/system/tools/aidl/aidl_language_l.cpp system/tools/aidl/aidl_language_l.ll
```

Do this:
```
cp /usr/bin/flex prebuilts/misc/linux-x86/flex/flex-2.5.39
```

### python

If you get this:

```
SyntaxError: Missing parentheses in call to 'print'. Did you mean print(sys.stderr, "Usage: %s <Version.inc.in>")?
```

Then you must make `python` reference Python 2.x (instead of Python 3.x). The prebuilt Python bundled with AOSP doesn't include zlib support and fails during build, so we must do a trick to use the system Python 2.x under this command:

```
mkdir ~/ncr/aosp9/fakepython2
ln -s /usr/bin/python2 ~/ncr/aosp9/fakepython2/python

PATH="/home/lubos/ncr/aosp9/fakepython2/:$PATH"
```
