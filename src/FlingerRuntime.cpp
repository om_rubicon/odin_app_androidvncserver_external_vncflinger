#include "FlingerRuntime.h"

static char argv[] = "vncflinger";

FlingerRuntime::FlingerRuntime()
: android::AndroidRuntime(argv, sizeof(argv))
{
}

void FlingerRuntime::start()
{
	android::Vector<android::String8> args;
	android::AndroidRuntime::start("com.android.internal.os.RuntimeInit", args, false);
}

void FlingerRuntime::onStarted()
{
	int rv = mInnerMain();
	exit(rv);
}
