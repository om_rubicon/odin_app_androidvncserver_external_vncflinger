#ifndef INPUT_INJECTOR_H
#define INPUT_INJECTOR_H

#include <utils/RefBase.h>
#include <jni.h>

namespace android {

class InputInjector : public RefBase
{
public:
	InputInjector(JNIEnv* env);
	~InputInjector();

	void keyEvent(bool down, uint32_t key);
    void pointerEvent(int buttonMask, int x, int y);

	// For compat with InputDevice
	void stop() {}
	void reconfigure(int, int) {}
private:
	jobject vscrollEvent(float x, float y, float value);
	static void androidKeyCode(uint32_t vncKey, jint& androidKeyCode, jint& metaState);
private:
	JNIEnv* mEnv;
	jobject mInputManager;
	jmethodID mInjectInputEvent;
	jclass mMotionEvent, mKeyEvent;
	jmethodID mMotionEventObtain;
	jmethodID mMotionEventObtainForScroll;
	jmethodID mMotionEventSetSource;

	jmethodID mKeyEventCtor;

	jclass mMotionEventPointerCoords;
	jmethodID mMEPCCtor;
	jmethodID mMEPCSetAxisValue;
	jfieldID mMEPC_x, mMEPC_y;

	jclass mMotionEventPointerProperties;
	jmethodID mMEPPCtor;
	jfieldID mMEPP_id;

	int mPreviousButtonMask = 0;
};

}

#endif
