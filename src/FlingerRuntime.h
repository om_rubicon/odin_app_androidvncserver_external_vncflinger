#ifndef FLINGER_RUNTIME_H
#define FLINGER_RUNTIME_H
#include <android_runtime/AndroidRuntime.h>
#include <functional>

class FlingerRuntime : public android::AndroidRuntime
{
public:
	FlingerRuntime();
	void setInnerMain(std::function<int()> innerMain) { mInnerMain = innerMain; }
	void start();

	void onStarted() override;
private:
	std::function<int()> mInnerMain;
};

#endif
