#define LOG_TAG "InputInjector"
#include <utils/Log.h>

#include "InputInjector.h"
#include <utils/SystemClock.h>

namespace android
{

#define CHECKEXCEPTION()  if (mEnv->ExceptionCheck()) \
    { \
        mEnv->ExceptionDescribe(); \
		return; \
    }

InputInjector::InputInjector(JNIEnv* env)
: mEnv(env)
{
	jclass clsIM = mEnv->FindClass("android/hardware/input/InputManager");
    CHECKEXCEPTION();

	jmethodID mid = mEnv->GetStaticMethodID(clsIM, "getInstance", "()Landroid/hardware/input/InputManager;");
	CHECKEXCEPTION();
	jobject imInstance = mEnv->CallStaticObjectMethod(clsIM, mid);
	CHECKEXCEPTION();

	mInputManager = mEnv->NewGlobalRef(imInstance);
	mEnv->DeleteLocalRef(imInstance);

	mInjectInputEvent = mEnv->GetMethodID(clsIM, "injectInputEvent", "(Landroid/view/InputEvent;I)Z");
	CHECKEXCEPTION();

	// Get class KeyEvent
	jclass clsKE = mEnv->FindClass("android/view/KeyEvent");
	CHECKEXCEPTION();

	mKeyEvent = (jclass) mEnv->NewGlobalRef(clsKE);
	mEnv->DeleteLocalRef(clsKE);

	mKeyEventCtor = mEnv->GetMethodID(mKeyEvent, "<init>", "(JJIIIIIIII)V");
	CHECKEXCEPTION();

	// Get class MotionEvent
	jclass clsME = mEnv->FindClass("android/view/MotionEvent");
	CHECKEXCEPTION();

	mMotionEvent = (jclass) mEnv->NewGlobalRef(clsME);
	mEnv->DeleteLocalRef(clsME);

	mMotionEventObtain = mEnv->GetStaticMethodID(mMotionEvent, "obtain", "(JJIFFI)Landroid/view/MotionEvent;");
	CHECKEXCEPTION();

	mMotionEventObtainForScroll = mEnv->GetStaticMethodID(mMotionEvent, "obtain", "(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;");
	CHECKEXCEPTION();

	mMotionEventSetSource = mEnv->GetMethodID(mMotionEvent, "setSource", "(I)V");
	CHECKEXCEPTION();

	//

	jclass clsMEPC = mEnv->FindClass("android/view/MotionEvent$PointerCoords");
	CHECKEXCEPTION();

	mMotionEventPointerCoords = (jclass) mEnv->NewGlobalRef(clsMEPC);
	mEnv->DeleteLocalRef(clsMEPC);

	mMEPCCtor = mEnv->GetMethodID(mMotionEventPointerCoords, "<init>", "()V");
	CHECKEXCEPTION();

	mMEPCSetAxisValue = mEnv->GetMethodID(mMotionEventPointerCoords, "setAxisValue", "(IF)V");
	CHECKEXCEPTION();

	mMEPC_x = mEnv->GetFieldID(mMotionEventPointerCoords, "x", "F");
	CHECKEXCEPTION();

	mMEPC_y = mEnv->GetFieldID(mMotionEventPointerCoords, "y", "F");
	CHECKEXCEPTION();

	//

	jclass clsMEPP = mEnv->FindClass("android/view/MotionEvent$PointerProperties");
	CHECKEXCEPTION();

	mMotionEventPointerProperties = (jclass) mEnv->NewGlobalRef(clsMEPP);
	mEnv->DeleteLocalRef(clsMEPP);

	mMEPPCtor = mEnv->GetMethodID(mMotionEventPointerProperties, "<init>", "()V");
	CHECKEXCEPTION();

	mMEPP_id = mEnv->GetFieldID(mMotionEventPointerProperties, "id", "I");
}

InputInjector::~InputInjector()
{
	mEnv->DeleteGlobalRef(mInputManager);
	mEnv->DeleteGlobalRef(mKeyEvent);
	mEnv->DeleteGlobalRef(mMotionEvent);
	mEnv->DeleteGlobalRef(mMotionEventPointerCoords);
	mEnv->DeleteGlobalRef(mMotionEventPointerProperties);
}

void InputInjector::keyEvent(bool down, uint32_t key)
{
	// Since we're generating key press events one layer up above what InputDevice did, we need to produce Android key codes instead of
	// hardware scan code.
	jint keyCode, metaState;
	
	androidKeyCode(key, keyCode, metaState);

	if (keyCode == 0)
	{
		ALOGW("Unsupported key code: 0x%x", key);
		keyCode = 76;
		metaState = 1; // Shift+/ = ?
	}

	jlong now = uptimeMillis();
	jobject event = mEnv->NewObject(mKeyEvent, mKeyEventCtor, now, now, down ? 0 : 1, keyCode, 0, metaState, -1 /* KeyCharacterMap.VIRTUAL_KEYBOARD */,
		0 /* scan code */, 0, 0x101 /* SOURCE_KEYBOARD */);

	mEnv->CallBooleanMethod(mInputManager, mInjectInputEvent, event, 2 /* INJECT_INPUT_EVENT_MODE_WAIT_FOR_FINISH */);
	mEnv->DeleteLocalRef(event);
}

void InputInjector::androidKeyCode(uint32_t c, jint& androidKeyCode, jint& metaState)
{
	constexpr jint META_SHIFT_ON = 1;
	constexpr jint META_NUMLOCK_ON = 0x8f;

	metaState = 0;
	androidKeyCode = 0;

	if ('a' <= c && c <= 'z')
		androidKeyCode = 0x1d + (c - 'a');
    else if ('A' <= c && c <= 'Z')
	{
		androidKeyCode = 0x1d + (c - 'A');
		metaState = META_SHIFT_ON;
	}
	else if (c == '_')
	{
		androidKeyCode = 69;
		metaState = META_SHIFT_ON;
	}
	else if (c == '$')
	{
		androidKeyCode = 11;
		metaState = META_SHIFT_ON;
	}
	else if (c == '!')
	{
		androidKeyCode = 8;
		metaState = META_SHIFT_ON;
	}
	else if (c == '%')
	{
		androidKeyCode = 12;
		metaState = META_SHIFT_ON;
	}
	else if (c == '^')
	{
		androidKeyCode = 13;
		metaState = META_SHIFT_ON;
	}
	else if (c == '&')
	{
		androidKeyCode = 14;
		metaState = META_SHIFT_ON;
	}
	else if (c == '|')
	{
		androidKeyCode = 73;
		metaState = META_SHIFT_ON;
	}
	else if (c == '~')
	{
		androidKeyCode = 68;
		metaState = META_SHIFT_ON;
	}
	else if (c == '?')
	{
		androidKeyCode = 76;
		metaState = META_SHIFT_ON;
	}
	else if (c == '<')
	{
		androidKeyCode = 55;
		metaState = META_SHIFT_ON;
	}
	else if (c == '>')
	{
		androidKeyCode = 56;
		metaState = META_SHIFT_ON;
	}
	else if (c == ':')
	{
		androidKeyCode = 74;
		metaState = META_SHIFT_ON;
	}
	else if (c == '"')
	{
		androidKeyCode = 75;
		metaState = META_SHIFT_ON;
	}
	else if (c == '{')
	{
		androidKeyCode = 71;
		metaState = META_SHIFT_ON;
	}
	else if (c == '}')
	{
		androidKeyCode = 72;
		metaState = META_SHIFT_ON;
	}
	else if ('0' <= c && c <= '9')
		androidKeyCode = 7 + (c - '0');
	else if (c == 0xff0d) // ENTER
		androidKeyCode = 0x42;
	else if (c == 0xff08) // backspace
		androidKeyCode = 0x43;
	else if (c == 0xffff) // del
		androidKeyCode = 0x70;
	else if (c == 0xff50) // home
		androidKeyCode = 122;
	else if (c == 0xff57) // end
		androidKeyCode = 123;
	else if (c == 0xff52) // up
		androidKeyCode = 19;
	else if (c == 0xff54) // down
		androidKeyCode = 20;
	else if (c == 0xff51) // left
		androidKeyCode = 21;
	else if (c == 0xff53) // right
		androidKeyCode = 22;
	else if (c == 0xffaf) // numpad /
		androidKeyCode = 154;
	else if (c == 0xffab) // numpad +
		androidKeyCode = 157;
	else if (c == '+')
		androidKeyCode = 81;
	else if (c == 0xffad) // numpad -
		androidKeyCode = 156;
	else if (c == 0xffaa) // numpad *
		androidKeyCode = 155;
	else if (c == '-')
		androidKeyCode = 69;
	else if (c == '/')
		androidKeyCode = 76;
	else if (c == '*')
		androidKeyCode = 17;
	else if (c == 0xff8d) // numpad enter
		// androidKeyCode = 160; // this is the right code, but it doesn't work well
		androidKeyCode = 0x42; // generate normal enter
	else if (c == 0xffae) // numpad dot
		androidKeyCode = 158;
	else if (c == ',')
		androidKeyCode = 55;
	else if (c == '.')
		androidKeyCode = 56;
	else if (c == '#')
		androidKeyCode = 18;
	else if (c == '@')
		androidKeyCode = 77;
	else if (0xffb0 <= c && 0xffb9 >= c) // numpad numbers
	{
		// This is the right code range, but it doesn't work for me
		// androidKeyCode = 144 + (c - 0xffb0);
		// metaState = META_NUMLOCK_ON;

		androidKeyCode = 7 + (c - 0xffb0); // generate non-numpad key numbers
	}
	else if (c == ' ')
		androidKeyCode = 62;
	else if (c == '=')
		androidKeyCode = 70;
	else if (c == '`')
		androidKeyCode = 68;
	else if (c == '[')
		androidKeyCode = 71;
	else if (c == ']')
		androidKeyCode = 72;
	else if (c == '\\')
		androidKeyCode = 73;
	else if (c == ';')
		androidKeyCode = 74;
	else if (c == '(')
		androidKeyCode = 162;
	else if (c == ')')
		androidKeyCode = 163;
	else if (c == 0xff09) // TAB
		androidKeyCode = 61;
	else if (c == 0xff55) // page up
		androidKeyCode = 92;
	else if (c == 0xff56) // page down
		androidKeyCode = 93;
	else if (c == 0xff1b) // escape
		androidKeyCode = 111;
	else if (c == 0xff67) // context menu
		androidKeyCode = 82;
	else if (c == 0xffe1) // left shift
		androidKeyCode = 59;
	else if (c == 0xffe2) // right shift
		androidKeyCode = 60;
	else if (c == 0xffe3) // left ctrl
		androidKeyCode = 113;
	else if (c == 0xffe4) // right ctrl
		androidKeyCode = 114;
	else if (c == 0xffe9) // left alt
		androidKeyCode = 57;
	else if (c == 0xfe03) // right alt
		androidKeyCode = 58;
	else if (c == 0xffeb) // left meta
		androidKeyCode = 117;
	else if (c == 0xffec) // right meta
		androidKeyCode = 118;
	else if (c == 0xff13) // pause/break
		androidKeyCode = 121;
	else if (c == 0xfd1d) // print screen / sysrq
		androidKeyCode = 120;
	else if (0xffbe <= c && c <= 0xffc9) // F1 - F12
		androidKeyCode = 131 + (c - 0xffbe);
	else if (c == 251 || c == 252 || c == 3 || c == 4 || c == 26 || c == 187) // side buttons (left and right), home button, back button, power button, app switch button
		androidKeyCode = c;
}

void InputInjector::pointerEvent(int buttonMask, int x, int y)
{
	jlong now = uptimeMillis();
	jobject event;

	if (buttonMask & 0x1)
	{
		jint action = 0; // ACTION_DOWN

		// We need to generate ACTION_MOVE when dragging
		if (mPreviousButtonMask & 0x1)
			action = 2; // ACTION_MOVE

		event = mEnv->CallStaticObjectMethod(mMotionEvent, mMotionEventObtain, now, now, action, (jfloat) x, (jfloat) y, 0);

		mEnv->CallVoidMethod(event, mMotionEventSetSource, 0x00001002 /* SOURCE_TOUCHSCREEN */);
		mEnv->CallBooleanMethod(mInputManager, mInjectInputEvent, event, 2 /* INJECT_INPUT_EVENT_MODE_WAIT_FOR_FINISH */);
		mEnv->DeleteLocalRef(event);
	}
	else if (mPreviousButtonMask & 0x1)
	{
		event = mEnv->CallStaticObjectMethod(mMotionEvent, mMotionEventObtain, now, now, 1 /* ACTION_UP */, (jfloat) x, (jfloat) y, 0);

		mEnv->CallVoidMethod(event, mMotionEventSetSource, 0x00001002 /* SOURCE_TOUCHSCREEN */);
		mEnv->CallBooleanMethod(mInputManager, mInjectInputEvent, event, 2 /* INJECT_INPUT_EVENT_MODE_WAIT_FOR_FINISH */);
		mEnv->DeleteLocalRef(event);
	}
	if (buttonMask & 0x8)
	{
		event = vscrollEvent(x, y, 1);
		mEnv->CallBooleanMethod(mInputManager, mInjectInputEvent, event, 2 /* INJECT_INPUT_EVENT_MODE_WAIT_FOR_FINISH */);
		mEnv->DeleteLocalRef(event);
	}
	if (buttonMask & 0x10)
	{
		event = vscrollEvent(x, y, -1);
		mEnv->CallBooleanMethod(mInputManager, mInjectInputEvent, event, 2 /* INJECT_INPUT_EVENT_MODE_WAIT_FOR_FINISH */);
		mEnv->DeleteLocalRef(event);
	}

	mPreviousButtonMask = buttonMask;
}

// https://stackoverflow.com/a/48021736/479753
jobject InputInjector::vscrollEvent(float x, float y, float value)
{
	// MotionEvent.PointerCoords coord = new MotionEvent.PointerCoords();
	jobject coord = mEnv->NewObject(mMotionEventPointerCoords, mMEPCCtor);

	// coord.x = x;
	mEnv->SetFloatField(coord, mMEPC_x, x);

	// coord.y = y;
	mEnv->SetFloatField(coord, mMEPC_y, y);

	// coord.setAxisValue(MotionEvent.AXIS_VSCROLL, 1f);
	mEnv->CallVoidMethod(coord, mMEPCSetAxisValue, 9 /* AXIS_VSCROLL */, value);

	// MotionEvent.PointerCoords[] coords = { coord };
	jobjectArray coords = mEnv->NewObjectArray(1, mMotionEventPointerCoords, coord);
	mEnv->DeleteLocalRef(coord);

	// MotionEvent.PointerProperties properties = new MotionEvent.PointerProperties();
	jobject properties = mEnv->NewObject(mMotionEventPointerProperties, mMEPPCtor);

	// properties.id = 0;
	mEnv->SetIntField(properties, mMEPP_id, 0);

	// MotionEvent.PointerProperties[] prop = { properties };
	jobjectArray prop = mEnv->NewObjectArray(1, mMotionEventPointerProperties, properties);
	mEnv->DeleteLocalRef(properties);

	// MotionEvent scrollEvent = MotionEvent.obtain(
    // SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_SCROLL,
    // 1, prop, coords, 0, 0, 1f, 1f, 0, 0, SOURCE_CLASS_POINTER, 0);
	jlong now = uptimeMillis();

	jobject event = mEnv->CallStaticObjectMethod(mMotionEvent, mMotionEventObtainForScroll, now, now, 8 /* ACTION_SCROLL */,
		1, prop, coords, 0, 0, 1.0f, 1.0f, 0, 0, 2 /* SOURCE_CLASS_POINTER */, 0);

	mEnv->DeleteLocalRef(prop);
	mEnv->DeleteLocalRef(coords);

	return event;
}


}